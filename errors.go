package cygses

import "errors"

/*Internal Errors*/

/*Validation errors*/
var errBadRemoteAddress = errors.New("the current remote address does not match the remote address of the session")
var errBadUserAgent = errors.New("the current user agent does not match the user agent of the session")
var errDeadSession = errors.New("the current session is no longer active")
var errSessionNotSet = errors.New("the session id is not associated with any session")
var errExpiredShortSession = errors.New("the short lifespan session has expired")
var errInvalidSessionPassword = errors.New("the rotating session key is invalid")

/*External errors*/

//Returned if overseer is running and it is called again.
var ErrOverseerRunning = errors.New("the overseer is already running")

//Returned if a session value is accessed but not set
var ErrValueNotSet = errors.New("this session variable was not set")
