package cygses

import (
	"log"
	"net/http"
	"time"
)

//GetSession returns a Session for the current user.
//If a session does not exist, one will be created and returned.
func GetSession(w http.ResponseWriter, r *http.Request) *Session {
	ua := r.Header.Get("User-Agent")
	ra := r.RemoteAddr
	isNewSession := false

	//Retrieve the sessionStore ID
	cygid, e := r.Cookie("CYGID")

	//If the cookie isn't set, create a new sessionStore
	if e == http.ErrNoCookie {
		cygid = buildNewSession(ua, ra, w)
		isNewSession = true
	}

	//validate the session
	ses := getSesPointer(sessionBin.bin[sessionName(cygid.Value)])
	e, valid := validate(ses, r, sessionName(cygid.Value), sessionPassword(cygid.Value))
	if !valid || ses == nil {
		//If the session is invalid, build a new one.
		oldCookieValue := sessionName(sessionName(cygid.Value))
		cygid = buildNewSession(ua, ra, w)
		ses = getSesPointer(sessionBin.bin[sessionName(cygid.Value)])
		if Settings.VerboseErrors {
			log.Println("Session "+oldCookieValue+": ", e)
		}
	} else {

		//Extend short sessions
		if !isNewSession {
			//If it's a short lifespan session and auto expansion is enabled, expand the length.
			if Settings.UseShortAuth && Settings.AutoExpandShortSes {
				ses.shortExpires = time.Now().Add(Settings.ShortLength)
			}
		} else {
			resetSessionPassword(ses, sessionName(cygid.Value), w)
		}

		//Rotate the session cookie.

	}

	//return the Session.
	return ses
}

//Runs overseer 1 time if AutoOverseer is disabled and infinitely
//if it is enabled. This option can be changed at any point.
func RunOverseer() error {
	if !overseerRunning {
		go overseer(true)
		return nil
	}
	return ErrOverseerRunning
}
