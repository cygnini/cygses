package cygses

import "time"

//SettingsStruct is a struct used to set the settings of cygses.
type SettingsStruct struct {
	SessionLength      time.Duration //The length of a session.
	UseShortAuth       bool          //Enable short lifespan session authentication.
	ShortLength        time.Duration //The length of the short lifespan session
	AutoExpandShortSes bool          //Expands short session every time getSession is called.
	AutoOverseer       bool          //Specify if overseer should run automatically. Note that if you disable thing, you will have to call RunOverseer() again.
	OverseerInterval   time.Duration //Specify the interval in which overseer is run if AutoOverseer is true
	ValidateRemoteAddr bool          //Validates sessions by using the Remote Address.
	VerboseErrors      bool          //Logs internal errors for invalid sessions.
}

//Stores settings for the entire library to interact with
var Settings SettingsStruct
