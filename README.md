#Get started

Cygses is a simplistic session management library written for Go. Cygses sets out to correct the pain points of web development in Go. Though other libraries do exist, Cygses is mean to be simplistic and familiar to web developers by using influences for popular back-end web languages. **There will likely be dramatic and code breaking changes in version 0. Do not use this in production code unless you plan on adapting to these changes.**

##Installation

`$ go get bitbucket.org/cygnini/cygses/`

##Documentation
For documentation, visit <https://godoc.org/bitbucket.org/cygnini/cygses>

##Information

Let me know how you guys like this, what I can do to improve it or add to it. You can contact me at giovanni.panaro@cygnini.org. Feel free to send donations on Google Pay to giovanni.panaro@cygnini.org. Currently I'm a full time college student and a full time worker and this is a major time commitment. Anything helps.