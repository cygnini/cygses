package cygses

import (
	"github.com/google/uuid"
	"net/http"
	"sync"
	"time"
)

type values map[string]*string
type sessionStore map[string]*sesTable

//sesTable contains all values associated with a session.
type sesTable struct {
	remoteAddr   string     //IP address of the client
	userAgent    string     //User agent of client (sessions are not valid cross-browser)
	expiration   time.Time  //Expiration time of session
	valTable     values     //Public session values
	dead         bool       //If the session is sill valid, this is false
	set          bool       //Value set to false by default to prevent clients from creating their own session tokens
	shortExpires time.Time  //If short lifespan session authentication is enabled, this is the short expiration. This value refreshes won getSession()
	mux          sync.Mutex //Only one session can be accessed at a time
	sessionPword string     //A rotating session string. This is kept separate from the actual session string so that the session is indexable in overseer.
}

//sbc is a struct that defines a bin for containing a group of sessions
//and a mutual exclusion lock for all sessions in the group.
type sbc struct {
	bin sessionStore //All of the Sessions
}

var sessionBin sbc
var scope chan string
var scopeLock sync.Mutex
var overseerRunning bool

func init() {
	//Make required structures for cygses to function
	sessionBin.bin = make(map[string]*sesTable)
	scope = make(chan string, 1)

	//Preset the settings
	Settings = SettingsStruct{
		UseShortAuth:       false,
		ShortLength:        time.Minute * 5,
		SessionLength:      time.Hour * 24,
		AutoExpandShortSes: true,
		AutoOverseer:       true,
		OverseerInterval:   0,
		ValidateRemoteAddr: false,
		VerboseErrors:      true,
	}

	//This go routine will be killed when AutoOverseer is disabled.
	go overseer(false)
}

//The overseer is a concurrent process who's only job is to kill expired sessions.
//The reason it exists it a creative way of killing abandon sessions. This way
//if a user generates a session and leaves it, it can still be removed from memory.
func overseer(overseerCalled bool) {
	overseerRunning = true
	for Settings.AutoOverseer || overseerCalled {
		overseerCalled = false
		scopeLock.Lock()
		for i := 0; i < len(scope); i++ {
			//lock the session
			select {
			case currentString := <-scope:
				currentSes := getSesPointer(sessionBin.bin[currentString])
				currentSes.mux.Lock()
				if currentSes.expiration.Before(time.Now()) || currentSes.dead {
					//Delete all references to the session, leave for garbage collection.
					delete(sessionBin.bin, currentString)
					currentSes = nil
				} else {
					//If the session is still valid, add it to the end of the channel to be looked at later
					scope <- currentString
					currentSes.mux.Unlock()
				}
			default:
				//Do nothing. This just makes the channel grab non-blocking.
			}
			//Prevent the process from taking up too much of the CPU.
			time.Sleep(time.Millisecond * 100)
		}
		if Settings.AutoOverseer {
			time.Sleep(Settings.OverseerInterval)
		}
		scopeLock.Unlock()
	}
	overseerRunning = false
}

func generateUUID() string {
	name := uuid.New()
	return name.String()
}

//newSession unconditionally creates a new session and returns a session cookie.
func newSession(ra, ua string) http.Cookie {
	//Create the sessionUname for the cookie
	sessionUname := generateUUID()
	sessionPword := generateUUID()
	//Add the sessionStore to the overseer's scope
	//Create a new entry in the SessionBin and initialize sesTable and values
	expiry := time.Now().Add(Settings.SessionLength)
	sessionBin.bin[sessionUname] = &sesTable{
		remoteAddr:   ra,
		userAgent:    ua,
		expiration:   expiry,
		valTable:     make(map[string]*string),
		dead:         false,
		set:          true,
		shortExpires: time.Now().Add(Settings.ShortLength),
		sessionPword: sessionPword,
	}

	//If the channel is full, create a new channel with double the cap
	//and set scope equal the new channel.
	scopeLock.Lock()
	if len(scope) == cap(scope) {
		go func() {
			tmpChan := make(chan string, cap(scope)*2)
			for len(scope) != 0 {
				tmpChan <- <-scope
			}
			scope = tmpChan
		}()
	}
	scope <- sessionUname
	scopeLock.Unlock()

	//return the sessionStore id cookie
	return http.Cookie{
		Name:    "CYGID",
		Value:   sessionUname + sessionPword,
		Expires: expiry,
		Path:    "/",
	}
}

func resetSessionPassword(ses *Session, sessionName string, w http.ResponseWriter) {
	newPassword := generateUUID()
	ses.sessionPword = newPassword
	http.SetCookie(w, &http.Cookie{
		Name:    "CYGID",
		Value:   sessionName + newPassword,
		Expires: ses.expiration,
		Path:    "/",
	})
}

//Unconditionally builds a new session and replaces the CYGID cookie
func buildNewSession(ua, ra string, w http.ResponseWriter) *http.Cookie {
	sesCookie := newSession(ra, ua)
	http.SetCookie(w, &sesCookie)
	return &sesCookie
}

//Validates a session, returns errBadSession if the session is invalid.
func validate(ses *Session, r *http.Request, name string, password string) (error, bool) {

	//lock the session until it is validated
	ses.mux.Lock()
	defer ses.mux.Unlock()

	//Validate the session is not a fake session token
	if !ses.set {
		return errSessionNotSet, false
	}

	if ses.sessionPword != password {
		return errInvalidSessionPassword, false
	}

	//Validate the client has the same IP address as the session
	if r.RemoteAddr != ses.remoteAddr && Settings.ValidateRemoteAddr {
		return errBadRemoteAddress, false
	}

	//Validate the client is using the same browser as the session
	if r.UserAgent() != ses.userAgent {
		return errBadUserAgent, false
	}

	//Validate the session is still alive
	if ses.dead {
		return errDeadSession, false
	}

	//Validate that the session had not expired
	if ses.expiration.Before(time.Now()) {
		//Setting the session as dead makes it easier for the overseer to remove it.
		ses.dead = true
		return errDeadSession, false
	}

	//Check to see if the short lifespan session is expired
	if Settings.UseShortAuth && ses.shortExpires.Before(time.Now()) {
		delete(sessionBin.bin, name)
		ses = nil
		return errExpiredShortSession, false
	}

	//The session has passed all tests, it is valid
	return nil, true
}

func getSesPointer(table *sesTable) *Session {
	return (*Session)(table)
}

func sessionName(sesString string) string {
	return sesString[0:36]
}

func sessionPassword(sesString string) string {
	return sesString[36:72]
}
