package cygses

import "time"

//Used for users wanting to interact with sessions.
type Session sesTable

//Returns specified session value
func (ses *Session) Get(s string) (error, string) {
	ses.mux.Lock()
	defer ses.mux.Unlock()
	returnString := ses.valTable[s]
	if returnString == nil {
		return ErrValueNotSet, ""
	}
	return nil, *ses.valTable[s]
}

//Sets a session value
func (ses *Session) Set(string, value string) {
	ses.mux.Lock()
	defer ses.mux.Unlock()
	ses.valTable[string] = &value
}

//Kills a session. On the next GetSession(), a new session will be generated for the user.
func (ses *Session) Kill() {
	ses.mux.Lock()
	defer ses.mux.Unlock()
	ses.dead = true
}

//Refresh the expiration of a short lifespan session. You only need to use
//this if Settings.AutoExpandShortSes = false.
func (ses *Session) ExpandShortSession() {
	ses.mux.Lock()
	defer ses.mux.Unlock()
	ses.shortExpires = time.Now().Add(Settings.ShortLength)
}
